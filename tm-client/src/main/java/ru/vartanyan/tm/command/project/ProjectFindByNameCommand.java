package ru.vartanyan.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.command.AbstractProjectCommand;
import ru.vartanyan.tm.endpoint.Project;
import ru.vartanyan.tm.endpoint.ProjectDTO;
import ru.vartanyan.tm.endpoint.Session;
import ru.vartanyan.tm.endpoint.SessionDTO;
import ru.vartanyan.tm.exception.system.NotLoggedInException;
import ru.vartanyan.tm.exception.system.NullObjectException;
import ru.vartanyan.tm.util.TerminalUtil;

@Component
public class ProjectFindByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-find-by-name";
    }

    @Override
    public String description() {
        return "Find project by name";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO session = bootstrap.getSession();
        if (session == null) throw new NotLoggedInException();
        System.out.println("[FIND PROJECT BY NAME]");
        System.out.println("[ENTER NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.findProjectByName(name, session);
        System.out.println(project.getName() + ": " + project.getDescription());
    }

}
