package ru.vartanyan.tm.command;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.vartanyan.tm.endpoint.AdminEndpoint;
import ru.vartanyan.tm.endpoint.SessionEndpoint;
import ru.vartanyan.tm.endpoint.UserEndpoint;

public abstract class AbstractUserCommand extends AbstractCommand{

    @NotNull
    @Autowired
    public AdminEndpoint adminEndpoint;

}
